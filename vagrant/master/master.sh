#! /bin/bash

sudo apt-get update

sudo apt install -y git

sudo apt install -y cmake

sudo apt install -y g++

sudo apt install -y libboost-dev

sudo apt install -y libpcap-dev

sudo apt install -y libzmq3-dev

sudo apt install -y libpugixml-dev

sudo apt install -y sshpass

sudo apt-get install -y nmap

sudo apt-get upgrade -y

sudo apt-get autoremove -y

cd /home/vagrant/d-streamon/streamon

rm -f CMakeCache.txt

cmake .

make

cd zmq-client

g++ -o zmq_client zmq_client.cpp -lzmq

cd ../../

sudo apt install -y curl

sudo apt install -y build-essential

sudo apt install -y libssl-dev

sudo apt install -y mongodb-server

sudo apt install -y redis-server

sudo apt install -y ansible

curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.32.1/install.sh | bash

export NVM_DIR="/home/vagrant/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"

nvm install v0.10.25

npm config set registry="http://registry.npmjs.org/"

npm install

cd

npm install -g swagger

npm install -g gulp

ssh-keygen -N "" -f $HOME/.ssh/id_rsa

sudo sed -ie 's/#host_key_checking/host_key_checking/g' /etc/ansible/ansible.cfg

sudo sed -ie 's/#   StrictHostKeyChecking ask/StrictHostKeyChecking no/g' /etc/ssh/ssh_config
