#! /bin/bash

sudo apt-get update

sudo apt install -y libboost-dev

sudo apt install -y libpcap-dev

sudo apt install -y libzmq3-dev

sudo apt install -y unzip

sudo sed -ie 's/PermitRootLogin without-password/PermitRootLogin yes/g' /etc/ssh/sshd_config

sudo service ssh restart

sudo apt-get autoremove -y
