#!/bin/bash


INGRESS_ETH="veth1_b.22"

TEST_LEN=10

for i in {0..30}

do

RX_PKT_START=$(ifconfig ${INGRESS_ETH}|grep 'RX packets' | awk '{gsub("packets:","",$2); print $2}')

sleep ${TEST_LEN}


RX_PKT_END=$(ifconfig ${INGRESS_ETH}|grep 'RX packets' | awk '{gsub("packets:","",$2); print $2}')



TOTAL_RX=$((${RX_PKT_END} - ${RX_PKT_START}))

echo "total RX "${TOTAL_RX}

echo "total pps "$((${TOTAL_RX}/${TEST_LEN}))

done
