#! /bin/bash

modprobe pktgen

function pgset() {
  local result
  echo $1 > $PGDEV
  result=`cat $PGDEV | fgrep "Result: OK:"`
  if [ "$result" = "" ]; then
    cat $PGDEV | fgrep Result:
  fi
}

function pg() {
  echo inject > $PGDEV
  cat $PGDEV
}
# Config Start Here -----------------------------------------------------------

ETH="veth1_a.11"

DST_IP="10.0.2.2"

DST_MAC="52:54:00:cc:67:89"

#DST_MAC="00:00:00:00:00:0b"

PKT_SIZE="pkt_size 60"

# thread config

# Each CPU has own thread. Two CPU exammple. We add eth1, eth2 respectivly.

PGDEV=/proc/net/pktgen/kpktgend_0

echo "Removing all devices"

pgset "rem_device_all"

echo "Adding ${ETH}"

pgset "add_device ${ETH}"


# device config

CLONE_SKB="clone_skb 1000000"

# COUNT 0 means forever

COUNT="count 0"

#COUNT="count 10000000"

PGDEV=/proc/net/pktgen/${ETH}

echo "Configuring $PGDEV"

pgset "$COUNT"

pgset "$CLONE_SKB"

pgset "$PKT_SIZE"

pgset "dst ${DST_IP}"

pgset "dst_mac ${DST_MAC}"


# Time to run

PGDEV=/proc/net/pktgen/pgctrl

echo "Running... ctrl^C to stop"

pgset "start"

echo "Done"
